// Item number 2
db.users.find(
{
	$or: [
	{
		firstName: {
			$regex: "S"
		}
	},
	{
		lastName: {
			$regex: 'D'
		}
	}
	]
},
{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);

// Item number 3
db.users.find(
{
	$and: [
	{department: "HR"},
	{age: {$gte:70}}

	]
}
);

// Item number 4
db.users.find(
	{
		$and: [
			{firstName: {
				$regex: "e"}
			},
			{
				age: {
				$lte: 30
				}
			}

		]
	}
);
